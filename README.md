# Simple Web App

## Setup

### Update of `.gitlab-ci.yml`

First, clone this project into a sub-group. Then update the `variables` section of `.gitlab-ci.yml` with the name of the sub-group:

```yaml
variables:
  CI_PARENT_GROUP: k33g-demos
```

### GitLab Personal AccessToken

- Create a GitLab Personal Access Token
- Create a CI variable named `GITLAB_PAT` (`Settings | CI/CD | Variables`)
  - the value of `GITLAB_PAT` is the GitLab Personal Access Token
  - unprotect the variable
  - mask the variable
  - `GITLAB_PAT` is used to publish the files to the generic package registry

## How to use the demo

### 0- Create some labels

- `feature::start` (*change with "begin" ?*)
- `feature::end`

### 1- Change the `main.js` file

Do some changes into `src/web/main.js`. 

For example, replace:
```javascript
let output = getMessage("Hey 👋 I'm Bob! 😀")
```
By:
```javascript
let output = getMessage("Hello 🖖 I'm Spock! 👽")
```

- Then commit you changes on the `main` branch
- Look at the pipeline list (`Build | Pipelines`)
- Look at the details of the running pipeline
- When the pipeline is finished, go to the environments list (`Operate | Environments`)
  - You should get an environment named `production-simple-web-app-main`
  - Clik on the <kbd>Open</kbd> button to visualize the deployed web application

### 2- Change the `main.js` file, again...

```javascript
let output = getMessage("Hello 🖖 I'm Spock! 👽")
```
By:
```javascript
let output = getMessage("Hello 🤗 I'm Jane Doe! 👩")
```

- Then commit you changes on a **new branch** to create a **Merge Request**
- Look at the pipeline list (`Build | Pipelines`)
- Look at the details of the running pipeline (there is a `test` job)
- When the pipeline is finished, got to the Merge Request (`Code | Merge Request`)
  - you can see the unit-tests widget
  - you can clik on the <kbd>View app</kbd> button to visualize the deployed web **review** application
- Go to the environments list (`Operate | Environments`)
  - There are 2 environments (the production one, and the review-app one)

Once you are happy with your changes, merge the Merge Requests.
- It will trigger the removal of the review environment
- It will deploy your change to the productin environment
- Once the deployment finsihed, return to the environments list (`Operate | Environments`)
  - There is only the production environment

### 3- Create a release

- Create a tag:
  - Go to `Code | Tags`
  - Click on the <kbd>New tag</kbd> button
    - Give a tag name: `N.N.N` (*replace N with numbers*)
    - Create the tag from the `main` branch
    - Add some text to the `message` field
    - Click on the <kbd>Create tag</kbd> button
- A new pipeline will start
- When the pipeline is finished:
  - Go to `Deploy | Package Registry`
    - Click on a record(row), you'll get the detail of the published package
  - Go to `Deploy | Release`
    - You can see the list of the created releases

