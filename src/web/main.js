
function getMessage(text, version, about) {
    //return `{"message":"👋 Hello World 🌍 from TinyGo 🤗🥶🥵","input": "${text}"}`
    return {
        message: "👋 Hello World 🌍 From GitLab",
        input: text,
        version: version,
        about: about
    }
}

//let jsonOutput = getMessage("Hey 👋 I'm Bob Morane! 😀", "v0.0.4")
let jsonOutput = getMessage("Hey 👋 I'm Jane Doe! 🙋‍♀️", "v0.0.3", "made with 💜 and 🍵 on GitLab 🦊")

console.log(jsonOutput)

document.querySelector("h1").innerText = jsonOutput.input
document.querySelector("h2").innerText = jsonOutput.message
document.querySelector("h3").innerText = jsonOutput.version
document.querySelector("h4").innerText = jsonOutput.about


