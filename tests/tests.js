import { readFileSync, writeFileSync } from 'fs'

let failureTestCase = (testName, message) => 
`<testcase classname="test_case" name="${testName}">
	<failure message="${message}"/>
</testcase>`

let successTestCase = (testName) => 
`<testcase classname="test_case" name="${testName}"></testcase>`

let testSuitesTemplate = (xmlTestCases, tests, failures) => {
	return `<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
	<testsuite name="junit-report" tests="${tests}" failures="${failures}" errors="0" skipped="0">
	${xmlTestCases}
	</testsuite>
</testsuites>
`
}

var testCases = []
var tests = 0
var failures = 0

function testCaseFunction(functionName, testCase) {

	let success = testCase()

	tests+=1

	if(success) {
		testCases.push(successTestCase(`Test of ${functionName} num: ${tests}`))
	} else {
		testCases.push(failureTestCase(`Test of ${functionName} num: ${tests}`, `😡 Call of ${functionName} | result: ${result}`))
		failures+=1
	}	
}


testCaseFunction("say_hello", () => {
	console.log("🤖 Tests of say_hello")
	let result = "Bob Morane"
	if(result=="Bob Morane") {
		return true
	} else {
		return false
	}
})

testCaseFunction("say_hi", () => {
	console.log("🤖 Tests of say_hi")
	let result = "Jane Doe"

	if(result=="Jane Doe") {
		return true
	} else {
		return false
	}
})

testCaseFunction("say_hello_world", () => {
	console.log("🤖 Tests of say_hello_world")
	let result = "John Doe"

	if(result=="John Doe") {
		return true
	} else {
		return false
	}
})

testCaseFunction("bonjour_tout_le_monde", () => {
	console.log("🤖 bonjour à tous")
	let result = "Bob Morane"

	if(result=="Bob Morane") {
		return true
	} else {
		return false
	}
})



console.log(testCases)

writeFileSync('./test-results.xml', testSuitesTemplate(testCases.join("\n"), tests, failures))



